while True:
	low = input("Введите нижний порог: ")
	up = input("Введите верхний порог: ")
	if not low.isdigit() or not up.isdigit():
		continue
	low = int(low)
	up = int(up)
	if low == 1:
		low = 2
	if low < 0 or up < 0:
		print("Числа не должны быть отрицательными!")
		continue
	if low >= up:
		print("Нижний порог должен быть меньше верхнего!")
		continue
	sum = 0
	flag_is_nat = 1
	for i in range(low, up + 1):
		flag_is_nat = 1
		for j in range(1, i // 2 + 1):
			if i % j == 0 and j != 1:
				flag_is_nat = 0
				break
		if flag_is_nat == 1:
			sum += i
	print("Сумма = ", sum)
	break
