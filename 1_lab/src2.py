while True:
	msg = input("Ведите текст: ")
	flag_is_rus = 1
	msg = msg.lower()
	const_list_of_v = ['а', 'е', 'и', 'о', 'у', 'ы', 'э', 'ю', 'я', 'ё']
	d_vow = set()
	vow_count = 0
	cons_count = 0
	for ch in msg:
		if ord(ch) == ord(' '):
			continue
		if ord(ch) < ord('а') or ord(ch) > ord('я'):
			if ord(ch) != ord('ё'):
				flag_is_rus = 0
				print("Текст должен содержать только киррилические буквы!")
				break
		if ch in const_list_of_v:
			vow_count += 1
			if ch not in d_vow:
				d_vow.add(ch)
		else:
			cons_count += 1
	if flag_is_rus:
		print("Количество гласных букв = ", vow_count, "Количество согласных букв = ", cons_count)
		print("Количество слов в тексте: ", len(msg.split(' ')))
		if vow_count == cons_count:
			print("Список всех гласных букв: ", d_vow)
		break

