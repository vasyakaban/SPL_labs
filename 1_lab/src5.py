names = ["Торт", "Пирожное", "Маффин"]
info = [[["Молоко", "Яйца", "Масло", "Сгущёное молоко", "Сахар"], 100, 600], [["Молоко", "Яйца", "Масло", "Какао", "Ванилин", "Шоколад"], 150, 900], [["Молоко", "Яйца", "Масло", "Изюм", "Клюква", "Шоколад"], 75, 750]]

products = dict(zip(names, info))

buy_list = {"Торт" : 0, "Пирожное" : 0, "Маффин" : 0}

choose = 0
choose_str = ""

while True:
	print("Выберите действие:")
	print("1. Просмотр состава продукции")
	print("2. Просмотр цены продукции")
	print("3. Просмотр количества продукции")
	print("4. Просмотр всей информации о продукции")
	print("5. Покупка")
	print("6. Выход")

	a = input()
	if a.isdigit():
		num = int(a)
	else:
		continue


	if num == 1:
		while True:
			choose_str = input("Введите наименование продукции(Торт, Пирожное, Маффин): ")
			choose_str = choose_str.capitalize()
			if products.get(choose_str) == None:
				print("Введите корректное наименование продукции!")
				continue
			else:
				print("Состав:", end='')
				print(products.get(choose_str)[0])
				break
	elif num == 2:
		while True:
			choose_str = input("Введите наименование продукции(Торт, Пирожное, Маффин): ")
			choose_str = choose_str.capitalize()
			if products.get(choose_str) == None:
				print("Введите корректное наименование продукции!")
				continue
			else:
				print("Цена за 100 грамм: ", end='')
				print(products.get(choose_str)[1])
				break

	elif num == 3:
		while True:
			choose_str = input("Введите наименование продукции(Торт, Пирожное, Маффин): ")
			choose_str = choose_str.capitalize()
			if products.get(choose_str) == None:
				print("Введите корректное наименование продукции!")
				continue
			else:
				print("Количество граммах: ", end='')
				print(products.get(choose_str)[2])
				break

	elif num == 4:
		print("Состав торта: ", products.get("Торт")[0])
		print("Цена за 100 грамм торта: ", products.get("Торт")[1])
		print("Количество торта в граммах: ", products.get("Торт")[2])

		print("Состав пирожного: ", products.get("Пирожное")[0])
		print("Цена за 100 грамм пирожного: ", products.get("Пирожное")[1])
		print("Количество пирожного в граммах: ", products.get("Пирожное")[2])

		print("Состав маффина: ", products.get("Маффин")[0])
		print("Цена за 100 грамм маффина: ", products.get("Маффин")[1])
		print("Количество маффина в граммах: ", products.get("Маффин")[2])

	elif num == 5:
		while True:
			choose_str = input("Введите наименование продукции(Торт, Пирожное, Маффин) или 'Выход' для выхода: ")
			choose_str = choose_str.capitalize()
			if choose_str == "Выход":
				break
			elif products.get(choose_str) == None:
				print("Введите корректное наименование продукции!")
				continue
			else:
				while True:
					count = ""
					count = input("Введите количество продукции в граммах: ")
					if count.isdigit():
						num = int(count)
						if num < 1:
							print("Количество должно быть > 0!")
							continue
						elif num > products.get(choose_str)[2]:
							print("Количество для покупки превышает количество на складе!")
							continue
						else:
							buy_list[choose_str] += num
							products[choose_str][2] -= num
							break
					else:
						continue
			print("Итоговая корзина:")
			print("Торт: ", buy_list["Торт"], " грамм, ценой ", buy_list["Торт"] / 100 * products["Торт"][1])
			print("Пирожное: ", buy_list["Пирожное"], " грамм, ценой ", buy_list["Пирожное"] / 100 * products["Пирожное"][1])
			print("Маффин: ", buy_list["Маффин"], " грамм, ценой ", buy_list["Маффин"] / 100 * products["Маффин"][1])

			print("Остаток на складе:")
			print("Торт: ", products["Торт"][2], " грамм")
			print("Пирожное: ", products["Пирожное"][2], " грамм")
			print("Маффин: ", products["Маффин"][2], " грамм")

	elif num == 6:
		print("До свидания!")
		exit()
