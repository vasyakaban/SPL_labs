def is_palindrome(text):
	flag_is_pal = False
	try:
		if len(text) == 0:
			raise Exception("Zero length text!")
		rev_text = text.lower()[::-1]
		if rev_text == text.lower():
			flag_is_pal = True
	except Exception as e:
		print(e)
	finally:
		return flag_is_pal

msg = input("Введите текст: ")
if is_palindrome(msg):
	print("Строка", msg, "- палиндром")
else:
	print("Строка", msg, "- не палиндром")
