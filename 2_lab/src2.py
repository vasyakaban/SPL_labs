def make_work(arg):
	try:
		if isinstance(arg, tuple):
			try:
				if len(arg) == 0:
					raise Exception("Zero length tuple!")

				count = 0
				for i in arg:
					if isinstance(i, str):
						sub_str = i.split()
						for j in sub_str:
							if not j.isdigit():
								count += len(j)

				print("Суммарная длина строк в кортеже = ", count)
			except Exception as e:
				print(e)
		elif isinstance(arg, list):
			try:
				if len(arg) == 0:
					raise Exception("Zero length list!")

				num_count = 0
				chr_count = 0

				bad_sym = "`1234567890-=\][';/.,~!@#$%^&*()_+|}{:?><"

				for i in arg:
					if isinstance(i, int):
						num_count += 1
					elif isinstance(i, str):
						if i.isdigit():
							num_count += 1
						else:
							chr_count += len(i)
							for j in bad_sym:
								for k in i:
									if j == k:
										chr_count -= 1
					else:
						continue

				print("Кол-во чисел в списке = ", num_count)
				print("Кол-во букв в списке = ", chr_count)
			except Exception as e:
				print(e)
		elif isinstance(arg, int):
			arg = abs(arg)
			count = 0
			while arg != 0:
				if (arg % 10) % 2 == 1:
					count += 1
				arg //= 10
			print("Кол-во нечётных цифр = ", count)
		elif isinstance(arg, str):
			bad_sym = "`1234567890-=\][';/.,~!@#$%^&*()_+|}{:?><"
			try:
				if len(arg) == 0:
					raise Exception("Zero length string!")

				chr_count = len(arg)
				for i in bad_sym:
					for j in arg:
						if j == i:
							chr_count -= 1

				print("Кол-во букв в строке = ", chr_count)
			except Exception as e:
				print(e)
		else:
			raise Exception("Wrong data type!")

	except Exception as e:
		print(e)

msg = "8934723gyiry73g"
make_work(msg)
