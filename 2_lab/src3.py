
def make_mat(mat):
	try:
		if len(mat) == 0:
			raise Exception("Zero matrix found!")
		for i in range(len(mat)):
			if len(mat) != len(mat[i]):
				raise Exception("Matrix is not qudratic!")

		for i in range(len(mat)):
			for j in range(len(mat)):
				if i == j:
					mat[i][j] = 1
				elif j > i:
					mat[i][j] = 0
				else:
					mat[i][j] = 2

	except Exception as e:
		print(e)

	finally:
		return mat


while True:
	msg = input("Введите размер матрицы: ")
	if not msg.isdigit():
		continue
	else:
		msg = int(msg)

	arr = [None] * msg

	for i in range(msg):
		arr[i] = [None] * msg

	arr = make_mat(arr)

	for i in range(len(arr)):
		for j in range(len(arr[i])):
			print(arr[i][j], end=" ")
		print("")
	break

