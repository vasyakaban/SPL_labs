import random

def fill_mat(mat):
	try:
		if len(mat) == 0:
			raise Exception("Matrix size is zero!")
	except Exception as e:
		print(e)
	else:
		for i in range(len(mat)):
			for j in range(len(mat[i])):
				mat[i][j] = random.randint(-1000, 1000)
	finally:
		return mat

def print_mat(mat):
	for i in mat:
		for j in i:
			print("%4d" % j, end=" ")
		print("")

mat = []
key_is_created = 0

while True:
	print("1. Создание матрицы и заполнение её случайнми числами")
	print("2. Перезаполнение матрицы случайными числами")
	print("3. Вывод матрицы на экран")
	print("4. Поиск максимального и минамального элементов матрицы")
	print("5. Выход")

	try:
		msg = int(input("Выберите действие: "))
	except ValueError:
		print("Введите число!")
		continue

	if msg > 5 or msg < 1:
		print("Число вне допустимого диапозона!")
		continue

	if msg == 1:
		while True:
			try:
				row = int(input("Введите кол-во строк: "))
				col = int(input("Введите кол-во колонок: "))
			except ValueError:
				print("Введите число!")
				continue

			if row < 1 or col < 1:
				print("Числа должны быть больше 1!")
				continue

			mat = [None] * row
			for i in range(len(mat)):
				mat[i] = [None] * col

			mat = fill_mat(mat)
			key_is_created = 1

			print("Созданная матрица: ")
			print_mat(mat)
			break

	elif msg == 2:
		if not key_is_created:
			print("Сначала создайте матрицу!")
			continue
		else:
			mat = fill_mat(mat)
			print("Перезаполненная матрица: ")
			print_mat(mat)

	elif msg == 3:
		if not key_is_created:
			print("Сначала создайте матрицу!")
			continue
		else:
			print_mat(mat)

	elif msg == 4:
		if not key_is_created:
			print("Сначала создайте матрицу!")
			continue

		min = mat[0][0]
		max = mat[0][0]

		for i in mat:
			for j in i:
				if min > j:
					min = j
				if max < j:
					max = j

		print("Минимальный элемент = ", min, "Максимальный элемент = ", max)

	else:
		break
